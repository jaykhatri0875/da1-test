package com.dao.everyday_goodproducts.service;

import java.util.List;

import com.hsbc.everyday_goodproducts.dao.fooditemsDAO;
import com.hsbc.everyday_goodproducts.model.FoodItems;

public class fooditemsService {
	private fooditemsDAO daoFood = new fooditemsDAO();

	public FoodItems savefoodItems(String foodItemName,
			double foodItemPrice,
			int foodItemQuantity,
			String type) {
		FoodItems fooditem = new FoodItems(foodItemName, foodItemPrice, foodItemQuantity, type);
		FoodItems fooditemcreated = this.daoFood.addFoodItem(fooditem);
		return fooditemcreated;
		
	}
	
	public void deletefoodItem(int fooditemID) {
		this.daoFood.deleteFoodItem(fooditemID);
	}
	 
	public List<FoodItems> fetchall(){
		return this.daoFood.fetchAllFoodItems();
	}
	public FoodItems fetchItemByID(int itemID) {
		return this.fetchItemByID(itemID);
	}
	
}
