/* date : 24/9/20 
 * version : 1.0
 * @author: JAY KHATRI
 * Purpose:this is model layer of ApparalItems for Everyday Goodproducts PvtLtd;
 * 
 */

package com.hsbc.everyday_goodproducts.model;

public class ElectronicsItems {
	private int electronicsItemCode;
	private String electronicsItemName;
	private double electronicsItemPrice;
	private int electronicsItemQuantity;
	private int warrenty;
	private static int electronicsItemcounter = 10000;  //Apparalitem sequence starting from 10000
	
	
	public ElectronicsItems(String electronicsItemName,
						double electronicsItemPrice,
						int electronicsItemQuantity) {
		
		this.electronicsItemCode=++electronicsItemcounter;
		this.electronicsItemName = electronicsItemName;
		this.electronicsItemPrice = electronicsItemPrice;
		this.electronicsItemQuantity = electronicsItemQuantity;
	}


	public int getElectronicsItemCode() {
		return electronicsItemCode;
	}


	public String getElectronicsItemName() {
		return electronicsItemName;
	}


	public void setElectronicsItemName(String electronicsItemName) {
		this.electronicsItemName = electronicsItemName;
	}


	public double getElectronicsItemPrice() {
		return electronicsItemPrice;
	}


	public void setElectronicsItemPrice(double electronicsItemPrice) {
		this.electronicsItemPrice = electronicsItemPrice;
	}


	public int getElectronicsItemQuantity() {
		return electronicsItemQuantity;
	}


	public void setElectronicsItemQuantity(int electronicsItemQuantity) {
		this.electronicsItemQuantity = electronicsItemQuantity;
	}


	public int getWarrenty() {
		return warrenty;
	}


	public void setWarrenty(int warrenty) {
		this.warrenty = warrenty;
	}


}
