/* date : 24/9/20 
 * version : 1.0
 * @author: JAY KHATRI
 * Purpose:this is model layer of ApparalItems for Everyday Goodproducts PvtLtd;
 * 
 */

package com.hsbc.everyday_goodproducts.model;



public class ApparalItems {
	private int apparalItemCode;
	private String apparalItemName;
	private double apparalItemPrice;
	private int apparalItemQuantity;
	private static int apparalItemcounter = 2000;  //Apparalitem sequence starting from 2000
	private String size;
	private String type; 
	
	public ApparalItems(String apparalItemName,
						double apparalItemPrice,
						int apparalItemQuantity,
						String size) {
		
		this.apparalItemCode=++apparalItemcounter;
		this.apparalItemName = apparalItemName;
		this.apparalItemPrice = apparalItemPrice;
		this.apparalItemQuantity = apparalItemQuantity;
		this.size = size;
		
	}

	public int getApparalItemCode() {
		return apparalItemCode;
	}

	public String getApparalItemName() {
		return apparalItemName;
	}

	public void setApparalItemName(String apparalItemName) {
		this.apparalItemName = apparalItemName;
	}

	public double getApparalItemPrice() {
		return apparalItemPrice;
	}

	public void setApparalItemPrice(double apparalItemPrice) {
		this.apparalItemPrice = apparalItemPrice;
	}

	public int getApparalItemQuantity() {
		return apparalItemQuantity;
	}

	public void setApparalItemQuantity(int apparalItemQuantity) {
		this.apparalItemQuantity = apparalItemQuantity;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}
