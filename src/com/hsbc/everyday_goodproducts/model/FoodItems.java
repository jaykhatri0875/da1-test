/* date : 24/9/20 
 * version : 1.0
 * @author: JAY KHATRI
 * Purpose:this is model layer of fooditems for  Everyday Goodproducts PvtLtd;
 * 
 */

package com.hsbc.everyday_goodproducts.model;

import java.util.Date;

public class FoodItems {
	
	public int getFoodItemCode() {
		return foodItemCode;
	}


	private int foodItemCode;
	private String foodItemName;
	private double foodItemPrice;
	private int foodItemQuantity;
	private static int foodItemcounter = 100;   // fooditem sequence starting from 100
	private Date fooditemManufacturedate;
	private Date fooditemExpdate;
	private String fooditemtype; 
	
	
	public FoodItems(String foodItemName,
					double foodItemPrice,
					int foodItemQuantity,
					String type) {
		this.foodItemName = foodItemName;
		this.foodItemPrice = foodItemPrice;
		this.foodItemQuantity = foodItemQuantity;
		this.fooditemtype = type;
		this.foodItemCode = ++foodItemcounter;
	}


	public String getFoodItemName() {
		return foodItemName;
	}


	public void setFoodItemName(String foodItemName) {
		this.foodItemName = foodItemName;
	}


	public double getFoodItemPrice() {
		return foodItemPrice;
	}


	public void setFoodItemPrice(double foodItemPrice) {
		this.foodItemPrice = foodItemPrice;
	}


	public int getFoodItemQuantity() {
		return foodItemQuantity;
	}


	public void setFoodItemQuantity(int foodItemQuantity) {
		this.foodItemQuantity = foodItemQuantity;
	}


	public Date getFooditemManufacturedate() {
		return fooditemManufacturedate;
	}


	public void setFooditemManufacturedate(Date fooditemManufacturedate) {
		this.fooditemManufacturedate = fooditemManufacturedate;
	}


	public Date getFooditemExpdate() {
		return fooditemExpdate;
	}


	public void setFooditemExpdate(Date fooditemExpdate) {
		this.fooditemExpdate = fooditemExpdate;
	}
	
	
	
	
}
