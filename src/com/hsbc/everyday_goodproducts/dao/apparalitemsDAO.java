package com.hsbc.everyday_goodproducts.dao;

import java.util.ArrayList;
import java.util.List;

import com.hsbc.everyday_goodproducts.model.ApparalItems;
import com.hsbc.everyday_goodproducts.model.FoodItems;


public class apparalitemsDAO implements apparalitemInterface{
	
	private static List<ApparalItems> apparalItemList = new ArrayList<>();

	@Override
	public ApparalItems addApparalItem(ApparalItems apparalitem) {
		// TODO Auto-generated method stub
		this.apparalItemList.add(apparalitem);
		return apparalitem ;
	}

	@Override
	public void deleteApparalItem(int apparalID) {
		// TODO Auto-generated method stub
		for(ApparalItems ai:apparalItemList) {
			if(ai.getApparalItemCode()==apparalID) {
				this.apparalItemList.remove(ai);
			}
		}
	}

	@Override
	public List<ApparalItems> fetchAllApparalitems() {
		// TODO Auto-generated method stub
		return this.apparalItemList;
	}

	@Override
	public ApparalItems fetchApparalitemByID(int apparalitemID) {
		// TODO Auto-generated method stub
		for(ApparalItems ai:apparalItemList) {
			if(ai.getApparalItemCode()==apparalitemID) {
				return ai;
			}
		}
		return null;
	}

}
