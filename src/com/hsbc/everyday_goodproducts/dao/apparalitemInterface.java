package com.hsbc.everyday_goodproducts.dao;

import java.util.List;

import com.hsbc.everyday_goodproducts.model.ApparalItems;

public interface apparalitemInterface {
	public ApparalItems addApparalItem(ApparalItems apparalitem);
	public void deleteApparalItem(int apparalID);
	public List<ApparalItems> fetchAllApparalitems();
	public ApparalItems fetchApparalitemByID(int apparalitemID);
}
