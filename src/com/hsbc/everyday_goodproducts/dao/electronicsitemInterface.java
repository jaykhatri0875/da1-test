package com.hsbc.everyday_goodproducts.dao;

import java.util.List;

import com.hsbc.everyday_goodproducts.model.ElectronicsItems;
public interface electronicsitemInterface {
	public ElectronicsItems addElectronicsItem(ElectronicsItems electronicsItem);
	public void deleteElectronicsItem(int itemID);
	public List<ElectronicsItems> fetchAllElectronicsItems();
	public ElectronicsItems fetchElectronicsItemByID(int itemID);

}
