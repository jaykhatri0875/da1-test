package com.hsbc.everyday_goodproducts.dao;

import java.util.ArrayList;
import java.util.List;

import com.hsbc.everyday_goodproducts.model.ApparalItems;
import com.hsbc.everyday_goodproducts.model.ElectronicsItems;

public class electronicsitemDAO implements electronicsitemInterface {
	
	private static List<ElectronicsItems> electronicsItemList = new ArrayList<>();
	@Override
	public ElectronicsItems addElectronicsItem(ElectronicsItems electronicsItem) {
		// TODO Auto-generated method stub
		this.electronicsItemList.add(electronicsItem);
		
		return electronicsItem;
	}

	@Override
	public void deleteElectronicsItem(int itemID) {
		// TODO Auto-generated method stub
		for(ElectronicsItems ei:electronicsItemList) {
			if(ei.getElectronicsItemCode()==itemID) {
				this.electronicsItemList.remove(ei);
			}
		}
	}

	@Override
	public List<ElectronicsItems> fetchAllElectronicsItems() {
		// TODO Auto-generated method stub
		return this.electronicsItemList;
	}

	@Override
	public ElectronicsItems fetchElectronicsItemByID(int itemID) {
		// TODO Auto-generated method stub
		for(ElectronicsItems ei:electronicsItemList) {
			if(ei.getElectronicsItemCode()==itemID) {
				return ei;
			}
		}
		return null;
	}

}
