package com.hsbc.everyday_goodproducts.dao;
/* date : 24/9/20 
 * version : 1.0
 * @author: JAY KHATRI
 * Purpose:this is DATA ACCESS layer of fooditems for  Everyday Goodproducts PvtLtd;
 * 
 */

import java.util.ArrayList;
import java.util.List;


import com.hsbc.everyday_goodproducts.model.FoodItems;


public class fooditemsDAO implements fooditemInterface {
	private static List<FoodItems> foodItemsList = new ArrayList<>();
	
	@Override
	public FoodItems addFoodItem(FoodItems newFooditem) {
		this.foodItemsList.add(newFooditem);
		return newFooditem;
		
	}

	
	public void deleteFoodItem(int fooditemID) {
		for(FoodItems fi:foodItemsList) {
			if(fi.getFoodItemCode()==fooditemID) {
				this.foodItemsList.remove(fi);
			}
		}
	}

	@Override
	public List<FoodItems> fetchAllFoodItems() {
		return this.foodItemsList;
	}

	@Override
	public FoodItems fetchFoodItembyID(int foodItemID) {
		for(FoodItems fi : foodItemsList ) {
			if(fi.getFoodItemCode() == foodItemID) {
				return fi;
			}
		}
		
		return null;
	}
	
	
	
}
