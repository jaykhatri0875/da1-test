package com.hsbc.everyday_goodproducts.dao;
/* date : 24/9/20 
 * version : 1.0
 * @author: JAY KHATRI
 * Purpose:this is DATA ACCESS layer INTERFACE of fooditems for  Everyday Goodproducts PvtLtd;
 * 
 */
import java.util.List;

import com.hsbc.everyday_goodproducts.model.FoodItems;

public interface fooditemInterface {
	public FoodItems addFoodItem(FoodItems newFooditem);
	public void deleteFoodItem(int fooditemID);
	public List<FoodItems> fetchAllFoodItems();
	public FoodItems fetchFoodItembyID(int foodItemID);
	

}
